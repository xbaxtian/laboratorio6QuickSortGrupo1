#include <iostream>
#include <math.h>
#include<stdlib.h>
#include <string>
using namespace std;



struct nodo
{
	nodo *ant;
	int dato;
	nodo *sgte;
};

typedef struct nodo *Lista;

void Sort_shell(Lista &cab);
void Quick_Sort(Lista &cab);
void programa_lb06(Lista &cab);
string menu_lb06(Lista &cab);

bool comparar(Lista &a, Lista &b);
void mostrarElementos(Lista &cab);
Lista crearElemento(int valor);
void ingresarFinal(Lista &cab, int valor);
int convertir(string num, bool &bandera);

int main()
{

	Lista cab = NULL;
	programa_lb06(cab);

	system("pause");
	return 0;
}

void mostrarElementos(Lista &cab)
{
	Lista aux = cab;

	if (cab == NULL)
		cout << "Lista Vacia";
	else
	{
		while (aux->sgte != NULL)
		{
			cout << "|" << aux->dato;
			aux = aux->sgte;
		}
		cout << "|" << aux->dato;
	}

}
Lista crearElemento(int valor)
{
	Lista aux = new (struct nodo);
	aux->ant = NULL;
	aux->dato = valor;
	aux->sgte = NULL;

	return aux;
}
void ingresarFinal(Lista &cab, int valor)
{
	Lista nuevo = crearElemento(valor);
	Lista aux = cab;

	if (cab == NULL)
		cab = nuevo;
	else
	{
		while (aux->sgte != NULL)
		{
			aux = aux->sgte;
		}

		nuevo->ant = aux;
		aux->sgte = nuevo;

	}

}
int convertir(string num, bool &bandera)//La bandera me indica si fue correcto, con 1 -> fue un exito, con 0 -> fracaso
{
	int aux = 0;
	bandera = true;

	for (int i = 0; i < num.size(); i++)
	{

		if ((int)num.at(i) >= 48 && (int)num.at(i) <= 57)
		{
			aux += ((int)num.at(i) - 48) * pow(10, num.size() - (i + 1));
		}
		else
		{
			bandera = false;
			aux = -999;
			break;
		}

	}

	return aux;
}

string menu_lb06(Lista &cab)
{
	system("cls");
	string op;
	cout << endl;
        cout<<"\t浜様様様様様様様様様様様様様様様様様様様様様様様�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\tMENU\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t�\t1. Agregar elemento a la lista\t\t�"<<endl;
        cout<<"\t�\t2. Ordenar por Shell Sort\t\t�"<<endl;
        cout<<"\t�\t3. Ordenar por Quick Sort\t\t�"<<endl;
        cout<<"\t�\t4. Mostrar\t\t\t\t�"<<endl;
        cout<<"\t�\t5. Salir\t\t\t\t�"<<endl;
        cout<<"\t�\t\t\t\t\t\t�"<<endl;
        cout<<"\t藩様様様様様様様様様様様様様様様様様様様様様様様�\n"<<endl;
	cout << endl << endl << "\tLa lista actual es: " << endl;
	cout << "\t\t";
	mostrarElementos(cab);

	cout << "\n\n\tSeleccionar opcion: ";
	cin >> op;

	cin.ignore(256, '\n');
	return op;
}

void programa_lb06(Lista &cab)
{
	string op;
	do
	{

		op = menu_lb06(cab);



		if (op.size() > 1 || op.size() == 0)
			cout << "\tHas escrito demasiados caracteres!" << endl;
		else
		{
			if (op.compare("1") == 0)
			{
				cout << "\n\tHas elegido la opcion 1" << endl;
				string aux;
				bool f;
				int n;

				cout << "\n\n\tIngrese el numero: ";
				cin >> aux;

				n = convertir(aux, f);

				if (f == false)
				{
					cout << "\n\tHa escrito un numero invalido";
					//getch();
				}

				else
					ingresarFinal(cab, n);


			}
			else if (op.compare("2") == 0)
			{
				cout << "\n\tHas elegido la opcion 2" << endl;

				Sort_shell(cab);

			}
			else if (op.compare("3") == 0)
			{
				cout << "\n\tHas elegido la opcion 3" << endl;

				Quick_Sort(cab);


			}
			else if (op.compare("4") == 0)
			{
				mostrarElementos(cab);
			}
			else if (op.compare("5") == 0)
			{
				cout << "\n\tHas elegido la opcion 7..." << endl;
				cout << "\tprocediendo a salir..." << endl;
			}

		}

		cin.ignore(256, '\n');
	} while (op != "5");
}

void Sort_shell(Lista &cab)
{
	if (cab != NULL)
	{
		int n = 1;
		Lista aux1 = cab;
		Lista aux2 = aux1->sgte;
		bool bandera = false;

		while (aux2 != NULL)
		{
			aux2 = aux2->sgte;
			n++;
		}

		do
		{
			n = n / 2;
			int c = 1;

			aux1 = cab;
			aux2 = aux1->sgte;
			if (n != 0)
			{
				bandera = false;
				while (c  < n)
				{
					aux2 = aux2->sgte;
					c++;
				}

				while (aux2 != NULL)
				{
					if (aux1->dato > aux2->dato)
					{
						int aux = aux1->dato;
						aux1->dato = aux2->dato;
						aux2->dato = aux;
						bandera = true;
					}
					aux1 = aux1->sgte;
					aux2 = aux2->sgte;

				}

			}
			else
			{
				if (bandera == true)
				{
				while (aux2 != NULL)
				{
					if (aux1->dato > aux2->dato)
					{
						int aux = aux1->dato;
						aux1->dato = aux2->dato;
						aux2->dato = aux;
						bandera = true;
					}
					aux1 = aux1->sgte;
					aux2 = aux2->sgte;
				}

				}
				bandera = false;
			}

		} while (n != 0 || bandera == true);
	}
}

void Quick_Sort(Lista &cab)
{
	if (cab != NULL)
	{
		int contador = 0;
		int n = 2;
		int contador1 = 0;
		Lista aux = cab->sgte;
		Lista sombrero = NULL;
		Lista pivote;

		Lista cab_aux = cab;
		Lista final_aux = NULL;
		Lista cab_s2 = cab;

		int paso = 0;


		bool orientacion = true;

		while (aux->sgte != NULL)
		{
			aux = aux->sgte;
			n++;
		}

		if (n == 1)
			return;
		else
		{

			pivote = cab;
			sombrero = aux;

			while (true)
			{
				if (pivote == NULL || sombrero == NULL)
					return;


				orientacion = true;
				paso = 0;
				while (pivote != sombrero)
				{
					if (orientacion)
					{

						if (pivote->dato > sombrero->dato)
						{
							int temp = sombrero->dato;
							sombrero->dato = pivote->dato;
							pivote->dato = temp;

							Lista temp1 = sombrero;
							sombrero = pivote;
							pivote = temp1;
							orientacion = false;
						}
						else
						{
							sombrero = sombrero->ant;
							paso++;
						}

					}
					else
					{
						if (pivote->dato < sombrero->dato)
						{
							int temp = sombrero->dato;
							sombrero->dato = pivote->dato;
							pivote->dato = temp;

							Lista temp1 = sombrero;
							sombrero = pivote;
							pivote = temp1;

							orientacion = true;
						}
						else
							sombrero = sombrero->sgte;
					}
				}
				contador1++;
				contador++;
				if (contador1 == 1)
					cab_aux = pivote;
				sombrero = pivote->ant;
				pivote = cab_s2;


				if (sombrero == final_aux)
				{
					if (cab_aux != cab_s2)
						contador = contador + paso;

					cab_s2 = cab_aux->sgte;

					final_aux = cab_aux;
					pivote = final_aux->sgte;
					sombrero = aux;



					contador1 = 0;

				}


			}
		}
	}
}
